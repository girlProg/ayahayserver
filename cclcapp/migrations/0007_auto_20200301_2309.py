# Generated by Django 3.0.3 on 2020-03-01 23:09

from decimal import Decimal
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cclcapp', '0006_studentreview'),
    ]

    operations = [
        migrations.AlterField(
            model_name='studentreview',
            name='metric',
            field=models.DecimalField(blank=True, choices=[('Neutral', 'Neutral'), ('Positive', 'Positive'), ('Negative', 'Negative')], decimal_places=1, default=Decimal('0'), max_digits=10),
        ),
        migrations.AlterField(
            model_name='studentreview',
            name='review',
            field=models.CharField(blank=True, default='', max_length=1500, null=True),
        ),
    ]
