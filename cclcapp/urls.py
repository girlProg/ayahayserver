from rest_framework import routers
from . import api_views, views
from django.conf.urls import  url, include
from ayahayServer import settings
from django.conf.urls.static import static



router = routers.DefaultRouter()
router.register('students', api_views.StudentViewSet, 'student',)
router.register('geolocation', api_views.GeoLocationViewSet, 'geolocation',)
router.register('country', api_views.CountryViewSet, 'country',)
router.register('state', api_views.StateViewSet, 'state',)
router.register('lg', api_views.LocalGovernmentViewSet, 'lg',)
router.register('ward', api_views.WardViewSet, 'ward',)
router.register('cluster', api_views.ClusterViewSet, 'cluster',)
router.register('classroom', api_views.ClassroomViewSet, 'classroom',)
router.register('assignment', api_views.AssignmentViewSet, 'assignment',)
router.register('assignmentgrade', api_views.AssignmentGradeViewSet, 'assignmentgrade',)
router.register('guardian', api_views.GuardianViewSet, 'guardian',)
router.register('user', api_views.UserViewSet, 'user',)
router.register('tutor', api_views.TutorsViewSet, 'tutor',)
router.register('curriculum', api_views.CurriculumViewSet, 'curriculum',)

# urlpatterns = router.urls

urlpatterns = [
    url(r'^', include(router.urls)),
    # url(r'^getstaff', views.convert_staff, name='convert_staff'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
