from . import serializers, models
from rest_framework import viewsets, permissions
# from .permissions import *


class AdminStaffPermissions(viewsets.ModelViewSet):
    # permission_classes = [permissions.IsAuthenticated, IsAccountsStaff, IsSchoolAccountActiveForStaff]
    pass



class GeoLocationViewSet(AdminStaffPermissions):
    serializer_class = serializers.GeoLocationSerializer
    queryset = models.GeoLocation.objects.all()


class UserViewSet(AdminStaffPermissions):
    serializer_class = serializers.UserSerializer
    queryset = models.User.objects.all()


class TutorsViewSet(AdminStaffPermissions):
    serializer_class = serializers.TutorSerializer
    queryset = models.Tutor.objects.all()


class CountryViewSet(AdminStaffPermissions):
    serializer_class = serializers.CountrySerializer
    queryset = models.Country.objects.all()


class StateViewSet(AdminStaffPermissions):
    serializer_class = serializers.StateSerializer
    queryset = models.State.objects.all()


class LocalGovernmentViewSet(AdminStaffPermissions):
    serializer_class = serializers.LocalGovernmentSerializer
    queryset = models.LocalGovernment.objects.all()


class WardViewSet(AdminStaffPermissions):
    serializer_class = serializers.WardSerializer
    queryset = models.Ward.objects.all()


class ClusterViewSet(AdminStaffPermissions):
    serializer_class = serializers.ClusterSerializer
    queryset = models.Cluster.objects.all()


class ClassroomViewSet(AdminStaffPermissions):
    serializer_class = serializers.ClassroomSerializer
    queryset = models.Classroom.objects.all()


class AssignmentViewSet(AdminStaffPermissions):
    serializer_class = serializers.AssignmentSerializer
    queryset = models.Assignment.objects.all()


class AssignmentGradeViewSet(AdminStaffPermissions):
    serializer_class = serializers.AssignmentGradeSerializer
    queryset = models.AssignmentGrade.objects.all()


class GuardianViewSet(AdminStaffPermissions):
    serializer_class = serializers.GuardianSerializer
    queryset = models.Guardian.objects.all()


class StudentViewSet(AdminStaffPermissions):
    serializer_class = serializers.StudentSerializer
    queryset = models.Student.objects.all()


class CurriculumViewSet(AdminStaffPermissions):
    serializer_class = serializers.CurriculumSerializer
    queryset = models.Curriculum.objects.all()

