from django.contrib import admin
import inspect
from django.db import models as django_models
from reversion.admin import VersionAdmin
from . import models
#
# for name,obj in inspect.getmembers(models):
#     if inspect.isclass(obj) and issubclass(obj,django_models.Model) and obj._meta.abstract is not True and name not in ['User']:
#         class Admin(admin.ModelAdmin):
#             def get_list_display(self,request):
#                 l = [t.name for t in self.model._meta.fields if t.editable]
#                 m = self.model.get_deferred_fields(self.model)
#                 return list(l)+list(m)
#             class Meta:
#                 model = obj
#         if name == "Token" or name == 'Student':
#             pass
#         else:
#             admin.site.register(obj,Admin)
#


class AssignmentGradeInline(admin.StackedInline):
    model = models.AssignmentGrade
    extra = 1


class CountryInline(admin.StackedInline):
    model = models.Country
    extra = 1


class StateInline(admin.StackedInline):
    model = models.State
    extra = 1


class LocalGovernmentInline(admin.StackedInline):
    model = models.LocalGovernment
    extra = 1

class GuardiantInline(admin.StackedInline):
    model = models.Guardian
    extra = 1


class ClassroomDayInfoInline(admin.StackedInline):
    model = models.ClassroomDayInfo
    extra = 1


class JobDescriptionInline(admin.StackedInline):
    model = models.JobDescription
    extra = 1


class JobRoleInline(admin.StackedInline):
    model = models.JobRole
    extra = 1


class StudentAdmin(VersionAdmin):
    # fieldsets = [ (None, {'fields': ['isAdult', 'classroom']})  , ]
    inlines = [AssignmentGradeInline, JobRoleInline]
    #list_display = ('number', 'tenant')


class CountryAdmin(VersionAdmin):
    # fieldsets = [ (None, {'fields': ['isAdult', 'classroom']}) , ]
    inlines = [StateInline]


class StateAdmin(VersionAdmin):
    # fieldsets = [  ]
    inlines = [LocalGovernmentInline]


class ClassroomAdmin(VersionAdmin):
    # fieldsets = [  ]
    inlines = [ClassroomDayInfoInline]


class EmployerAdmin(VersionAdmin):
    # fieldsets = [  ]
    inlines = [JobDescriptionInline,]


class JobDescriptionAdmin(VersionAdmin):
    # fieldsets = [  ]
    inlines = [JobRoleInline,]


admin.site.register(models.Student, StudentAdmin)
admin.site.register(models.Country, CountryAdmin)
admin.site.register(models.State, StateAdmin)
admin.site.register(models.Classroom, ClassroomAdmin)
admin.site.register(models.Employer, EmployerAdmin)
admin.site.register(models.JobDescription, JobDescriptionAdmin)
admin.site.register(models.Guardian)
admin.site.register(models.PhoneNumber)
admin.site.register(models.Cluster)
admin.site.register(models.Ward)
admin.site.register(models.LocalGovernment)
admin.site.register(models.GeoLocation)
admin.site.register(models.StudentReview)
admin.site.register(models.ClassroomDayInfo)
admin.site.register(models.Attendance)
admin.site.register(models.JobRole)
admin.site.register(models.Tutor)
admin.site.register(models.Curriculum)
admin.site.register(models.AssignmentGrade)
admin.site.register(models.Assignment)
admin.site.register(models.AvatarImage)
admin.site.register(models.Subject)
