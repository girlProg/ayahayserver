from django.db import models
from django.contrib.auth.models import  AbstractUser
from decimal import Decimal

CHOICES = (('Neutral', 'Neutral'), ('Positive', 'Positive'), ('Negative', 'Negative'))
ATTENDANCE_OPTIONS = (('Present', 'Present'), ('Absent', 'Absent'))

class BaseModel(models.Model):
    created=models.DateTimeField(auto_now_add=True,null=True)
    modified=models.DateTimeField(auto_now=True,null=True)
    class Meta:
        abstract=True
        ordering = ['-modified']


class AvatarImage(BaseModel):
    image = models.ImageField(blank=True,upload_to='Images/Avatars')


class GeoLocation(BaseModel):
    name = models.CharField(default='no name', null=True, blank=True, max_length=500)
    latitude = models.CharField(default='', null=True, blank=True, max_length=500)
    longitude = models.CharField(default='', null=True, blank=True, max_length=500)
    description = models.CharField(default='', null=True, blank=True, max_length=500)

    def __str__(self):
        return self.name


class User(AbstractUser):
    avatar = models.OneToOneField(AvatarImage,on_delete=models.CASCADE, null=True, blank=True, related_name='profile')
    isTutor = models.BooleanField(default=False)
    isAdmin = models.BooleanField(default=False)
    phone = models.CharField(blank=True,default='', max_length=500)
    dateJoined = models.DateTimeField(auto_now_add=False,null=True)
    dateLeft = models.DateTimeField(auto_now_add=False,null=True)
    address = models.CharField(blank=True,default='', max_length=500)
    age = models.IntegerField(default=0, null=True, blank=True)

    def __str__(self):
        return str(self.id) +' - '+self.username


class Tutor(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True, related_name='tutor')
    avatar = models.OneToOneField(AvatarImage,on_delete=models.CASCADE, null=True, blank=True, related_name='tutor')
    phone = models.CharField(blank=True,default='', max_length=500)
    dateJoined = models.DateTimeField(auto_now_add=False,null=True)
    dateLeft = models.DateTimeField(auto_now_add=False,null=True)
    address = models.CharField(blank=True,default='', max_length=500)
    age = models.IntegerField(default=0, null=True, blank=True)

    def __str__(self):
        return str(self.id) + ' - ' +self.user.username



class Country(BaseModel):
    name = models.CharField(default='Nigeria', null=True, blank=True, max_length=500)

    def __str__(self):
        return self.name

class State(BaseModel):
    name = models.CharField(default='', null=True, blank=True, max_length=500)
    country = models.ForeignKey(Country, null=True, blank=True, max_length=500, related_name='state', on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class LocalGovernment(BaseModel):
    name = models.CharField(default='', null=True, blank=True, max_length=500)
    state = models.ForeignKey(State, null=True, blank=True, on_delete=models.CASCADE, related_name='localgovernments')

    def __str__(self):
        return self.name


class Ward(BaseModel):
    name = models.CharField(default='', null=True, blank=True, max_length=500)
    localGovernment = models.ForeignKey(LocalGovernment, null=True, blank=True, default='', on_delete=models.CASCADE, related_name='wards')

    def __str__(self):
        return self.localGovernment.state.name + " - " + self.localGovernment.name + " - " + self.name


class Cluster(BaseModel):
    numberId = models.CharField(default='', null=True, blank=True, max_length=500)
    ward = models.ForeignKey(Ward, default='', null=True, blank=True, max_length=500, on_delete=models.CASCADE, related_name='clusters')

    def __str__(self):
        return self.numberId


class Classroom(BaseModel):
    numberId = models.CharField(default='', null=True, blank=True, max_length=500)
    location = models.CharField(default='', null=True, blank=True, max_length=500)
    geoLocation = models.ForeignKey(GeoLocation, on_delete=models.CASCADE, blank=True, null=True, related_name='classrooms')
    cluster = models.ForeignKey(Cluster, on_delete=models.CASCADE, blank=True, null=True, related_name='classrooms')
    tutor = models.ForeignKey(Tutor, on_delete=models.CASCADE, blank=True, null=True, related_name='classrooms')

    def __str__(self):
        return self.numberId


class Subject(BaseModel):
    name = models.CharField(default='', null=True, blank=True, max_length=500)

    def __str__(self):
        return f'{self.name}'


class Assignment(BaseModel):
    title = models.CharField(default='', null=True, blank=True, max_length=500)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, blank=True, null=True, related_name='assignment')
    document = models.FileField(blank=True,upload_to='Assignments/2021')
    date = models.DateTimeField(auto_now_add=False, null=True)

    def average_grade(self):
        total = 0
        count = 0
        for grade in self.assignmentgrade.all():
            total += grade.score
            count += 1
        return total/count

    def recorded_grades(self):
        return self.assignmentgrade.all().count

    def __str__(self):
        return self.title


class PhoneNumber(BaseModel):
    number = models.CharField(default='', null=True, blank=True, max_length=500)

    def __str__(self):
        return self.number


class Guardian(BaseModel):
    name = models.CharField(default='', null=True, blank=True, max_length=500)
    phone = models.ForeignKey(PhoneNumber, on_delete=models.CASCADE, blank=True, null=True, related_name='guardian')

    def __str__(self):
        return self.name


class Student(BaseModel):
    name = models.CharField(default='', null=True, blank=True, max_length=500)
    age = models.CharField(default='', null=True, blank=True, max_length=500)
    gender = models.CharField(choices=[('M', 'Male'), ('F', 'Female')], default='', null=True, blank=True, max_length=500)
    isAlmajiri = models.BooleanField(default=False, verbose_name='Tick if the student an Almajiri?')
    isAdult = models.BooleanField(default=False)
    classroom = models.ForeignKey(Classroom, on_delete=models.CASCADE, blank=True, null=True, related_name='student')
    guardian = models.ForeignKey(Guardian, on_delete=models.CASCADE, blank=True, null=True, related_name='student')
    avatar = models.OneToOneField(AvatarImage,on_delete=models.CASCADE, null=True, blank=True, related_name='student')

    def __str__(self):
        return  self.name


class AssignmentGrade(BaseModel):
    assignment = models.ForeignKey(Assignment, on_delete=models.CASCADE, blank=True, null=True, related_name='assignmentgrade')
    score = models.DecimalField(max_digits=10, decimal_places=1, default=Decimal.from_float(0.0), blank=True)
    student = models.ForeignKey(Student, on_delete=models.CASCADE, blank=True, null=True, related_name='assignmentgrade')

    def __str__(self):
        return f'{self.student.name} - {str(self.score)}'


class StudentReview(BaseModel):
    review = models.CharField(default='', null=True, blank=True, max_length=500)
    metric = models.DecimalField(max_digits=10, decimal_places=1, default=Decimal.from_float(0.0), blank=True, choices= CHOICES)
    student = models.ForeignKey(Student, on_delete=models.CASCADE, blank=True, null=True, related_name='review')

    def __str__(self):
        return self.metric


class Attendance(BaseModel):
    in_attendance = models.BooleanField(blank=True, null=True, default=False)
    student = models.ForeignKey(Student, on_delete=models.CASCADE, blank=True, null=True, related_name='attendance')
    date = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.student.name


class ClassroomDayInfo(BaseModel):
    information = models.CharField(default='', null=True, blank=True, max_length=1500)
    image = models.ImageField(upload_to='photoOfTheDay', default='', null=True, blank=True)
    metric = models.DecimalField(max_digits=10, decimal_places=1, default='', blank=True, choices= CHOICES)
    classroom = models.ForeignKey(Classroom, on_delete=models.CASCADE, blank=True, null=True, related_name='classroomreview')

    def __str__(self):
        return self.metric


class Employer(BaseModel):
    name = models.CharField(default='', null=True, blank=True, max_length=1500)
    type = models.CharField(default='', null=True, blank=True, max_length=1500)
    description = models.CharField(default='', null=True, blank=True, max_length=1500)

    def __str__(self):
        return self.name


class JobDescription(BaseModel):
    title = models.CharField(default='', null=True, blank=True, max_length=1500)
    notes = models.CharField(default='', null=True, blank=True, max_length=1500)
    employer = models.ForeignKey(Employer, on_delete=models.CASCADE, blank=True, null=True, related_name='jobdescription')

    def __str__(self):
        return self.title


class JobRole(BaseModel):
    job = models.ForeignKey(JobDescription, on_delete=models.CASCADE, blank=True, null=True, related_name='jobrole')
    student = models.ForeignKey(Student, on_delete=models.CASCADE, blank=True, null=True, related_name='jobrole')

    def __str__(self):
        return self.job.title


class Curriculum(BaseModel):
    document = models.FileField(blank=True,upload_to='Curriculum/')
    start_date = models.DateTimeField(auto_now_add=False,null=True)
    end_date = models.DateTimeField(auto_now_add=False,null=True)

    def __str__(self):
        return self.document


