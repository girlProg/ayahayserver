from rest_framework import serializers
from . import models
from drf_writable_nested.serializers import WritableNestedModelSerializer

# Serializer

class GeoLocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.GeoLocation
        exclude = ['created', 'modified']


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.User
        exclude = ['created', 'modified']


class TutorSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Tutor
        exclude = ['created', 'modified']


class AvatarSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.AvatarImage
        exclude = ['created', 'modified']


class PhoneNumberSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PhoneNumber
        exclude = ['created', 'modified']


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Country
        exclude = ['created', 'modified']


class StateSerializer(serializers.ModelSerializer):
    country = CountrySerializer()

    class Meta:
        model = models.State
        exclude = ['created', 'modified']


class LocalGovernmentSerializer(serializers.ModelSerializer):
    state = StateSerializer()

    class Meta:
        model = models.LocalGovernment
        exclude = ['created', 'modified']


class WardSerializer(serializers.ModelSerializer):
    localGovernment = LocalGovernmentSerializer()

    class Meta:
        model = models.Ward
        exclude = ['created', 'modified']


class ClusterSerializer(serializers.ModelSerializer):
    ward = WardSerializer()

    class Meta:
        model = models.Cluster
        exclude = ['created', 'modified']


class ClassroomSerializer(serializers.ModelSerializer):
    geoLocation = GeoLocationSerializer()
    cluster = ClusterSerializer()
    tutor = TutorSerializer()

    class Meta:
        model = models.Classroom
        exclude = ['created', 'modified']
        # fields = '__all__'


class AssignmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Assignment
        exclude = ['created', 'modified']


class GuardianSerializer(serializers.ModelSerializer):
    phone = PhoneNumberSerializer()

    class Meta:
        model = models.Guardian
        exclude = ['created', 'modified']


class StudentSerializer(serializers.ModelSerializer):
    classroom = ClassroomSerializer()
    guardian = GuardianSerializer()
    avatar = AvatarSerializer()

    class Meta:
        model = models.Student
        exclude = ['created', 'modified']


class AssignmentGradeSerializer(serializers.ModelSerializer):
    assignment = AssignmentSerializer()
    student = StudentSerializer()

    class Meta:
        model = models.AssignmentGrade
        exclude = ['created', 'modified']


class CurriculumSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Curriculum
        exclude = ['created', 'modified']

